import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Usuario } from '../class/usuario';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  
  public socketStatus:boolean = false;
  public usuario:Usuario;

  constructor(
    private socket: Socket,
    private router: Router
  ) {
    this.cargarStorage();
    this.checkStatus();
  }

  checkStatus(){
    this.socket.on('connect',()=>{
      console.log('Conectado al servidor');
      this.socketStatus = true;
      this.cargarStorage();
    });
    this.socket.on('disconnect',()=>{
      console.log('Desconectado al servidor');
      this.socketStatus = false;
    });
  }

  emit( evento:string, payload?: any, callback?:Function ){
    this.socket.emit( evento,payload,callback );
  }

  listen( evento:string ){
    return this.socket.fromEvent( evento );
  }
  
  getUsuario(){
    return this.usuario;
  }

  loginWS( nombre:string ){
    return new Promise((resolve,reject)=>{
      this.emit( 'configurar-usuario',{nombre},( resp )=>{
        this.usuario = new Usuario( nombre );
        this.guardarStorage();
        resolve()
      } );
    });
  }

  guardarStorage(){
    sessionStorage.setItem('usuario',JSON.stringify(this.usuario));
  }

  cargarStorage(){
    if( sessionStorage.getItem('usuario')){
      this.usuario = JSON.parse( sessionStorage.getItem('usuario') );
      this.loginWS( this.usuario.nombre );
    }
  }

  logoutWS(){
    this.usuario = null;
    sessionStorage.removeItem('usuario');
    const payload ={
      nombre:'sin-nombre'
    };
    this.emit('configurar-usuario',payload,()=>{});
    this.router.navigateByUrl('');
  }
}
